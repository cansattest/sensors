/*
 * spi.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>

void SPIInit (void)
{
	DDRB |= (1<<0)|(1<<1)|(1<<2)|(1<<4)|(1<<5)|(1<<6);
	PORTB |= (1<<4)|(1<<0)|(1<<5)|(1<<6);
	SPCR |= (1<<SPE)|(1<<MSTR)|(1<<SPR1)|(1<<CPOL)|(1<<CPHA);
}

void SPISend (uint8_t * send, uint8_t * receive, uint8_t count, uint8_t cs)
{
	uint8_t i;
	PORTB &= ~(1<<cs);
	for (i = 0; i < count; i++){
		SPDR = send[i];
		while (!(SPSR & (1 << SPIF)))
				;
		receive[i] = SPDR;

	}
	PORTB |= (1<<cs);
}
