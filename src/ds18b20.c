/*
 * ds18b20.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef ONW_PORT
	#define ONW_PORT PORTD
#endif

#ifndef ONW_PIN
	#define ONW_PIN PIND
#endif

#ifndef ONW_DDR
	#define ONW_DDR DDRD
#endif

#ifndef ONW_BIT
	#define ONW_BIT 2
#endif

uint8_t DS18Reset (void)
{
	uint8_t result = 0;
	ONW_DDR &= ~(1<<ONW_BIT);
	_delay_us (15);
	if (!(ONW_PIN & (1<<ONW_BIT)))
		return -1;
	ONW_DDR|=(1<<ONW_BIT);
	ONW_PORT &= ~(1<<ONW_BIT);
	_delay_us(490);
	ONW_DDR &= ~(1<<ONW_BIT);
	_delay_us (60);
	result =(ONW_PIN & (1<<ONW_BIT))>>ONW_BIT;
	_delay_us (480);
	return result;
}

void DS18Write0 (void)
{
	volatile uint8_t delay;
	ONW_DDR|=(1<<ONW_BIT);
	ONW_PORT &= ~(1<<ONW_BIT);
	_delay_us (60);
	ONW_DDR &= ~(1<<ONW_BIT);
	delay ++;
}

void DS18Write1 (void)
{
	volatile uint8_t delay;
	ONW_DDR|=(1<<ONW_BIT);
	ONW_PORT &= ~(1<<ONW_BIT);
	delay++;
	ONW_DDR &= ~(1<<ONW_BIT);
	_delay_us (61);
}

void DS18Write (uint8_t data)
{
	int i;
	for (i = 0; i<8; i++){
		if (data & 1)
			DS18Write1();
		else
			DS18Write0();
		data>>=1;
	}
}

uint8_t DS18Read (void)
{
	volatile uint8_t delay;
	uint8_t result = 0;
	int i;
	for (i = 0; i<8; i++){
		result>>=1;
		ONW_DDR|=(1<<ONW_BIT);
		ONW_PORT &= ~(1<<ONW_BIT);
		delay ++;
		ONW_DDR &= ~(1<<ONW_BIT);
		delay ++;
		if (ONW_PIN & (1<<ONW_BIT))
			result|=(1<<7);
		_delay_us(62);
	}
	return result;
}

