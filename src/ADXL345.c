/*
 * ADXL345.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <spi.h>
#include <i2c.h>

#define ADXL_SLAVE_ADDR 0x1D

#ifndef ADXL_CS
	#define ADXL_CS 4
#endif

void ADXLInit (void)
{
	uint8_t data[2];
/*	data[0] = 0x2C;
	data[1] = 0x08;
	SPISend (data, data, 2, ADXL_CS);
	data[0] = 0x2D;
	data[1] = 0x08;
	SPISend (data, data, 2, ADXL_CS);*/
	data[0] = 0x08;
	data[1] = 0x08;
	I2CWrite (ADXL_SLAVE_ADDR, 0x2C, data, 2);
/*	data[0] = 0x31;
	data[1] = 0x0B;
	SPISend (data, data, 2, ADXL_CS);*/
	data[0] = 0x0B;
	I2CWrite (ADXL_SLAVE_ADDR, 0x31, data, 1);
}

void ADXLGetData (uint8_t * data)
{
/*	uint8_t i;
	uint8_t tmp[7];
	tmp[0] = 0x32|0xC0;
	SPISend (tmp, tmp, 7, ADXL_CS);
	for (i = 0; i < 6; i++)
		data[i] = tmp[i+1];*/
	I2CRead (ADXL_SLAVE_ADDR, 0x32, data, 6);
}
