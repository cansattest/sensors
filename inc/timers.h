/*
 * timers.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef HEADERS_TIMERS_H_
#define HEADERS_TIMERS_H_

void TIM1Init (void);

void TIM1SetVal (uint32_t);
uint32_t TIM1GetVal (void);

#endif /* HEADERS_TIMERS_H_ */
