/*
 * BMP280.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef BMP280_H_
#define BMP280_H_

void BMPInit (void);
void BMPGetData (uint8_t *);
void BMPGetCalib (uint8_t *);

#endif /* BMP280_H_ */
