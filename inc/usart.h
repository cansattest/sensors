/*
 * usart.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef USART_H_
#define USART_H_

void USARTInit (void);
void USARTSend (uint8_t *, uint8_t);

#endif /* USART_H_ */
