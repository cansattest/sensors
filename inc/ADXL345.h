/*
 * ADXL345.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef INC_ADXL345_H_
#define INC_ADXL345_H_

void ADXLInit (void);
void ADXLGetData (uint8_t *);

#endif /* INC_ADXL345_H_ */
