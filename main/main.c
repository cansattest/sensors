/*
 * main.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>
#include <timers.h>
#include <spi.h>
#include <BMP280.h>
#include <ds18b20.h>
#include <ADXL345.h>
#include <util/delay.h>
#include <i2c.h>

#define F_CPU 1000000UL

int main (void)
{
	uint8_t result[42] = {'$', 'S', 'T', 'A'};
	uint32_t time_val;
	USARTInit();
	TIM1Init ();
	_delay_ms(500);
	I2CInit ();
	BMPInit ();
	ADXLInit();
	while (1){
		TIM1SetVal (0);
		time_val = 0;
		USARTSend (result, 42);
		BMPGetCalib (result + 4);
		BMPGetData (result + 28);
		ADXLGetData (result + 34);
		DS18Reset();
		DS18Write (DS18_SKIP_ROM);
		DS18Write (DS18_CONVERT_T);
		_delay_ms (760);
		DS18Reset();
		DS18Write (DS18_SKIP_ROM);
		DS18Write (DS18_READ_SCRATCH);
		result[40] = DS18Read();
		result[41] = DS18Read();
		while (time_val < 1000000UL)
			time_val = TIM1GetVal ();

	}
	return 0;
}
